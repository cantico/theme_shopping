; <?php/*

[general]
name							="theme_shopping"
version							="0.1.1"
description						="Skin Ovidentia"
description.fr					="Skin Ovidentia"
icon							="jaspe-logo.png"
image							="jaspe-mini.png"

delete							="1"
addon_access_control			="0"
ov_version						="8.1"
author							="Cantico"
encoding						="UTF-8"
mysql_character_set_database	="latin1,utf8"
php_version						="5.1.0"
addon_type						="THEME"
tags                            ="theme,shop"

[functionalities]
jquery="Available"


;*/ ?>